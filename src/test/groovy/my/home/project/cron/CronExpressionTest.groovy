package my.home.project.cron

import spock.lang.Specification

class CronExpressionTest extends Specification {

    def "should parse cron expression"() {
        when:
        def expression = CronExpression.parseExpression("*/15 0 1,15 * 1-5")

        then:
        expression.fields[0].values == [0, 15, 30, 45]
        expression.fields[1].values == [0]
        expression.fields[2].values == [1, 15]
        expression.fields[3].values == 1..12
        expression.fields[4].values == 1..5
    }

    def "should out cron expression result"() {
        given:
        def expression = CronExpression.parseExpression("*/15 0 1,15 * 1-5")
        def outputStream = new ByteArrayOutputStream()
        def ps = new PrintStream(outputStream, true)

        when:
        expression.out(ps, 14)

        then:
        outputStream.toString().strip() == """
minute        0 15 30 45
hour          0
day of month  1 15
month         1 2 3 4 5 6 7 8 9 10 11 12
day of week   1 2 3 4 5
        """.toString().strip()
    }

    def "should fail to parse cron expression if too many elements"() {
        when:
        CronExpression.parseExpression("*/15 0 1,15 * 1-5 *")

        then:
        thrown IllegalArgumentException
    }

    def "should fail to parse cron expression if too few elements"() {
        when:
        CronExpression.parseExpression("*/15 0 1,15 *")

        then:
        thrown IllegalArgumentException
    }
}
