package my.home.project.cron.elements

import spock.lang.Specification
import spock.lang.Unroll

import static my.home.project.cron.elements.ElementType.ALL
import static my.home.project.cron.elements.ElementType.PERIOD
import static my.home.project.cron.elements.ElementType.RANGE
import static my.home.project.cron.elements.ElementType.VALUE

class ElementTypeTest extends Specification {

    @Unroll
    def "Should failed to determine type of wrong element #wrongElement"(def wrongElement) {
        when:
        ElementType.getTypeByValue(wrongElement)

        then:
        thrown IllegalArgumentException

        where:
        wrongElement | _
        ""           | _
        "**"         | _
        "123"        | _
        "q1"         | _
        "1--2"       | _
        "1-"         | _
        "12/"        | _
        "1//2"       | _
        "/2"         | _
        "123/15"     | _
        "*/123"      | _
    }

    @Unroll
    def "Should  determine type as #expectedElementType for element #element"(def element, def expectedElementType) {
        when:
        def elementType = ElementType.getTypeByValue(element)

        then:
        elementType == expectedElementType

        where:
        element | expectedElementType
        "*"     | ALL
        "10"    | VALUE
        "10-15" | RANGE
        "*/15"  | PERIOD
        "5/15"  | PERIOD
    }
}
