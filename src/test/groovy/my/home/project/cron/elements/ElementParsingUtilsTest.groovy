package my.home.project.cron.elements

import my.home.project.cron.CronFieldConfig
import spock.lang.Specification

class ElementParsingUtilsTest extends Specification {

    def config = new CronFieldConfig("month", 1, 12)

    def "should parse all cron element"() {
        given:
        def element = "*"
        when:
        def parsedElement = ElementParsingUtils.parseElement(element, config)

        then:
        parsedElement instanceof AllElement
        parsedElement.values == 1..12
    }

    def "should parse value cron element"() {
        given:
        def element = "6"
        when:
        def parsedElement = ElementParsingUtils.parseElement(element, config)

        then:
        parsedElement instanceof ValueElement
        parsedElement.values == [6]
    }

    def "should fail to parse value[#value] cron element if out of range"(def value) {
        when:
        ElementParsingUtils.parseElement(value, config)

        then:
        thrown IllegalArgumentException

        where:
        value | _
        "0"   | _
        "13"  | _
    }

    def "should parse range cron element"() {
        given:
        def element = "1-5"
        when:
        def parsedElement = ElementParsingUtils.parseElement(element, config)

        then:
        parsedElement instanceof RangeElement
        parsedElement.values == 1..5
    }

    def "should fail to parse range[#value] cron element if values are out of range"(def value) {
        when:
        ElementParsingUtils.parseElement(value, config)

        then:
        thrown IllegalArgumentException

        where:
        value   | _
        "0-12"  | _
        "12-13" | _
    }

    def "should parse period cron element"() {
        given:
        def element = "*/3"
        when:
        def parsedElement = ElementParsingUtils.parseElement(element, config)

        then:
        parsedElement instanceof PeriodElement
        parsedElement.values == [1, 4, 7, 10]
    }

    def "should parse period cron element with start set"() {
        given:
        def element = "3/3"
        when:
        def parsedElement = ElementParsingUtils.parseElement(element, config)

        then:
        parsedElement instanceof PeriodElement
        parsedElement.values == [3, 6, 9, 12]
    }

    def "should fail to parse period[#value] cron element if values are out of range"(def value) {
        when:
        ElementParsingUtils.parseElement(value, config)

        then:
        thrown IllegalArgumentException

        where:
        value  | _
        "*/15" | _
        "0/12" | _
    }
}
