package my.home.project.cron

import spock.lang.Specification
import spock.lang.Unroll

class CronFieldTest extends Specification {

    def config = new CronFieldConfig("month", 1, 12)

    @Unroll
    def "should parse multi element field [#field]"(def field, def expectedValues) {
        when:
        def parsed = CronField.parseField(field, config)

        then:
        parsed.values == expectedValues

        where:
        field        | expectedValues
        "7,2,5"      | [2, 5, 7]
        "1,3-5"      | [1, 3, 4, 5]
        "4,3-5"      | [3, 4, 5]
        "1-3,6-8"    | [1, 2, 3, 6, 7, 8]
        "1-3,6-8,12" | [1, 2, 3, 6, 7, 8, 12]
    }

    @Unroll
    def "should failed parse multi element field [#field] if all or period elements are used"(def field) {
        when:
        CronField.parseField(field, config)

        then:
        thrown IllegalArgumentException

        where:
        field     | _
        "1,2/3"   | _
        "2/3,3-5" | _
        "*,6-8"   | _
        "*,6"     | _
        "*,2/3"   | _
    }
}
