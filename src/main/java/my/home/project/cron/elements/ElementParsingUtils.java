package my.home.project.cron.elements;

import my.home.project.cron.CronFieldConfig;

public class ElementParsingUtils {

    public static CronPatternElement parseElement(String element, CronFieldConfig config) {
        final var elementType = ElementType.getTypeByValue(element);
        switch (elementType) {
            case ALL:
                return new AllElement(config, element);
            case VALUE:
                return new ValueElement(config, element);
            case RANGE:
                return new RangeElement(config, element);
            case PERIOD:
                return new PeriodElement(config, element);
            default:
                throw new IllegalStateException("Unsupported element type: " + elementType);
        }
    }
}
