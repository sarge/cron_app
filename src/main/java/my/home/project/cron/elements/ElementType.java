package my.home.project.cron.elements;

import java.util.regex.Pattern;

public enum ElementType {
    ALL,
    VALUE,
    RANGE,
    PERIOD;

    private static final Pattern PERIOD_PATTERN = Pattern.compile("^(\\*|[0-9]{1,2})/[0-9]{1,2}$");
    private static final Pattern RANGE_PATTERN = Pattern.compile("^[0-9]{1,2}-[0-9]{1,2}$");
    private static final Pattern VALUE_PATTERN = Pattern.compile("^[0-9]{1,2}$");

    public static ElementType getTypeByValue(String elementValue) {
        final var value = elementValue.strip();
        if (value.equals("*")) {
            return ALL;
        } else if (RANGE_PATTERN.matcher(value).find()) {
            return RANGE;
        } else if (PERIOD_PATTERN.matcher(value).find()) {
            return PERIOD;
        } else if (VALUE_PATTERN.matcher(value).find()) {
            return VALUE;
        } else {
            throw new IllegalArgumentException("Cannot determine element type for value" + value);
        }

    }
}
