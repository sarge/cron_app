package my.home.project.cron.elements;

import my.home.project.cron.CronFieldConfig;

import java.util.List;
import java.util.stream.IntStream;

import static java.util.stream.Collectors.toUnmodifiableList;

public class PeriodElement extends CronPatternElement {

    public PeriodElement(CronFieldConfig config,
                         String element) {
        super(config, ElementType.PERIOD, element);
    }

    /**
     * Parse period element: * / 20
     */
    @Override
    List<Integer> parse(String element) {
        final var parts = element.split("/");
        final var startAt = parts[0].equals("*") ? config.lowerBound : Integer.parseInt(parts[0]);
        final var period = Integer.parseInt(parts[1]);
        if (startAt > period || startAt < config.lowerBound || period > config.upperBound) {
            throw new IllegalArgumentException("Cannot parse element in [" + config.name + "], " +
                    "period is not from from allowed range: " + element);
        }
        final var iterations = (config.upperBound - startAt) / period;
        return IntStream.range(0, iterations + 1)
                .map(val -> startAt + period * val)
                .boxed().collect(toUnmodifiableList());
    }
}
