package my.home.project.cron.elements;

import my.home.project.cron.CronFieldConfig;

import java.util.List;
import java.util.stream.IntStream;

import static java.util.stream.Collectors.toUnmodifiableList;

public class RangeElement extends CronPatternElement {

    public RangeElement(CronFieldConfig config,
                        String element) {
        super(config, ElementType.RANGE, element);
    }

    /**
     * Parse range element: e.g. 1-5
     */
    @Override
    List<Integer> parse(String element) {
        final var parts = element.split("-");
        final var from = Integer.parseInt(parts[0]);
        final var to = Integer.parseInt(parts[1]);
        if (from > to || from < config.lowerBound || to > config.upperBound) {
            throw new IllegalArgumentException("Cannot parse element in [" + config.name + "], " +
                    "values are not from allowed range: " + element);
        }
        return IntStream.range(from, to + 1).boxed().collect(toUnmodifiableList());
    }
}
