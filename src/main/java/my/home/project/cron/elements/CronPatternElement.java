package my.home.project.cron.elements;

import my.home.project.cron.CronFieldConfig;

import java.util.List;

public abstract class CronPatternElement {

    public final CronFieldConfig config;
    public final ElementType elementType;
    public final List<Integer> values;

    public CronPatternElement(CronFieldConfig config,
                              ElementType elementType,
                              String element) {
        this.config = config;
        this.elementType = elementType;
        this.values = parse(element);
    }

    abstract List<Integer> parse(String element);
}
