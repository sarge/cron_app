package my.home.project.cron.elements;

import my.home.project.cron.CronFieldConfig;

import java.util.List;

public class ValueElement extends CronPatternElement {

    public ValueElement(CronFieldConfig config,
                        String element) {
        super(config, ElementType.VALUE, element);
    }

    /**
     * Parse single value element: e.g. 15
     */
    @Override
    List<Integer> parse(String element) {
        final var value = Integer.parseInt(element);
        if (value < config.lowerBound || value > config.upperBound) {
            throw new IllegalArgumentException("Cannot parse element in [" + config.name + "], " +
                    "value is not from allowed range: " + element);
        }
        return List.of(value);
    }
}
