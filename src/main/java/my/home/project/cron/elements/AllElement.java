package my.home.project.cron.elements;

import my.home.project.cron.CronFieldConfig;

import java.util.List;
import java.util.stream.IntStream;

import static java.util.stream.Collectors.toUnmodifiableList;

public class AllElement extends CronPatternElement {

    public AllElement(CronFieldConfig config,
                      String element) {
        super(config, ElementType.ALL, element);
    }

    /**
     * Parse all element: *
     */
    @Override
    List<Integer> parse(String element) {
        return IntStream.range(config.lowerBound, config.upperBound + 1).boxed().collect(toUnmodifiableList());
    }
}
