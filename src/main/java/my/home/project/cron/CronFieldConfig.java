package my.home.project.cron;

public class CronFieldConfig {
    public final String name;
    public final int lowerBound;
    public final int upperBound;

    public CronFieldConfig(String name, int lowerBound, int upperBound) {
        this.name = name;
        this.lowerBound = lowerBound;
        this.upperBound = upperBound;
    }
}
