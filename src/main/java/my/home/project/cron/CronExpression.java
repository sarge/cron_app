package my.home.project.cron;

import com.google.common.collect.ImmutableList;

import java.io.PrintStream;
import java.util.List;
import java.util.stream.Collectors;

import static org.apache.commons.lang3.StringUtils.rightPad;

public class CronExpression {

    private final static CronFieldConfig MINUTES = new CronFieldConfig("minute", 0, 59);
    private final static CronFieldConfig HOURS = new CronFieldConfig("hour", 0, 23);
    private final static CronFieldConfig DAYS_OF_MONTH = new CronFieldConfig("day of month", 1, 31);
    private final static CronFieldConfig MONTH_OF_YEAR = new CronFieldConfig("month", 1, 12);
    private final static CronFieldConfig DAY_OF_WEEK = new CronFieldConfig("day of week", 0, 6);

    private final static CronFieldConfig[] CONFIGS =
            new CronFieldConfig[]{MINUTES, HOURS, DAYS_OF_MONTH, MONTH_OF_YEAR, DAY_OF_WEEK};

    public final List<CronField> fields;

    public CronExpression(List<CronField> fields) {
        this.fields = fields;
    }

    public static CronExpression parseExpression(String expression) {
        final var expressionParts = expression.strip().split(" ");
        return parseExpression(expressionParts);
    }

    public static CronExpression parseExpression(String[] expressionParts) {
        if (expressionParts.length != CONFIGS.length) {
            throw new IllegalArgumentException("Expression has wrong parts length: " + expressionParts.length);
        }

        final var cronFields = ImmutableList.<CronField>builder();
        for (int i = 0; i < expressionParts.length; i++) {
            cronFields.add(CronField.parseField(expressionParts[i], CONFIGS[i]));
        }
        return new CronExpression(cronFields.build());
    }

    public void out(PrintStream out, int nameColumnsLength) {
        fields.forEach(field -> {
            out.print(rightPad(field.config.name, nameColumnsLength));
            out.print(field.values.stream().map(String::valueOf).collect(Collectors.joining(" ")));
            out.println();
        });
    }
}
