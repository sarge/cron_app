package my.home.project.cron;

import my.home.project.cron.elements.ElementType;
import my.home.project.cron.elements.ElementParsingUtils;

import java.util.Arrays;
import java.util.List;

import static java.util.stream.Collectors.toUnmodifiableList;

public class CronField {

    public final CronFieldConfig config;
    public final List<Integer> values;

    public CronField(CronFieldConfig config, List<Integer> values) {
        this.config = config;
        this.values = values;
    }


    public static CronField parseField(String field, CronFieldConfig config) {
        final var parts = field.strip().split(",");
        final var elements = Arrays.stream(parts)
                .map(element -> ElementParsingUtils.parseElement(element, config)).collect(toUnmodifiableList());
        if (elements.size() > 1) {
            if (elements.stream()
                    .anyMatch(element -> element.elementType != ElementType.VALUE && element.elementType != ElementType.RANGE)) {
                throw new IllegalArgumentException("Cron field can have multiple elements only for values and ranges");
            }
        }
        return new CronField(config, elements.stream()
                .flatMap(element -> element.values.stream())
                .distinct().sorted().collect(toUnmodifiableList()));
    }
}
