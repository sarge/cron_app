package my.home.project;


import my.home.project.cron.CronExpression;

import java.util.Arrays;

import static org.apache.commons.lang3.StringUtils.rightPad;

public class AppMain {
    public static void main(String[] args) {
        if (args.length == 0 || args[0].isBlank()) {
            throw new IllegalArgumentException("No argument provided");
        }

        final var expressionArgument = args[0];
        final var parts = expressionArgument.split(" ");
        if (parts.length < 6) {
            throw new IllegalArgumentException("Provided argument is invalid, " +
                    "expected to have cron expression and command");
        }
        final var expressionValue = Arrays.copyOfRange(parts, 0, 5);
        final var commandValue = parts[5];

        CronExpression expression = CronExpression.parseExpression(expressionValue);

        final var outputColumns = 14;
        expression.out(System.out, outputColumns);
        System.out.println(rightPad("command", outputColumns) + commandValue);
    }
}
