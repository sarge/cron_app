# CRON parsing app
This application parses given CRON expression and write rules breakdown

## How to build
To build this app `Java SDK 11+` and `Maven` are required  
Run following commad from console:
```bash
mvn clean install
```

## How to use
After application is build successfully - it can be run from console:
```bash
sh cron_app.sh "*/15 0 1,15 * 1-5 /usr/bin/find"
```
**Note:** all arguments should be provided as single string